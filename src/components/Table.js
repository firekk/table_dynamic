import {render} from 'react-dom';
import React, {Component} from 'react';
import countries_data from '../data/countries.json';
import ReactPaginate from 'react-paginate';
import axios from 'axios';


class Table extends Component {

   state = {
      activePage: 1,
      sorting_order:"asc",
      col:'existing'
   }



   handlePageClick = (pageNumber) => {

        //console.log(pageNumber);
        this.setState({
           activePage: pageNumber.selected + 1
        });
     }


headerClick=(event,col_currently_clicked)=>{

      if(col_currently_clicked===this.state.col){

            if(this.state.sorting_order==="asc"){
               this.setState({
                  sorting_order:"desc"
               })
            } else{this.setState({sorting_order:"asc"})}
      }else{
            this.setState({
               col:col_currently_clicked,
               sorting_sorting_order:"asc"
            })
         }
      }


         rowClick=(event,rowData)=>{

            let chosen_lang=rowData.lc;
            //console.log(rowData.official_language);

            axios.get('https://glosbe.com/gapi/translate?from=pol&dest=eng&format=json&phrase=witaj&pretty=true').
            then(function(response){
               console.log(response.data);
            })

         }




   render() {

      //require("bootstrap/less/bootstrap.less");
      const axios = require('axios');
      const JsonTable = require('ts-react-json-table');
      let items = countries_data;

      if(this.state.sorting_order==="asc"){

         let instead_this=this;
         items.sort(function(a,b){
            return a[instead_this.state.col]>b[instead_this.state.col]?1:-1;
         })
      } else if(this.state.sorting_order==="desc"){

         let instead_this=this;
         items.sort(function(a,b){
            return a[instead_this.state.col]>b[instead_this.state.col]?-1:1;
         })
      }


      function range(per_page, number, data_table) {
         let start = (number - 1) * per_page;
         let finish = (per_page * number);
         let cutting_result = data_table.slice(start, finish);
         return cutting_result;
      }

      let table_display_part = range(4, this.state.activePage, items);

   return (<div>

      <div className="container-fluid" id="content_placeholder">
         <div>
            <JsonTable rows={table_display_part} onClickRow={this.rowClick}
            onClickHeader={this.headerClick}
              />
         </div>

         <div>
            <ReactPaginate
             previousLabel={"<"}
             nextLabel={">"}
             containerClassName={"pagination"}
             subContainerClassName={"pages pagination"}
             activeClassName={"factive"}
             pageCount={4} marginPagesDisplayed={2}
             pageRangeDisplayed={this.per_page}
             onPageChange={this.handlePageClick}
             />
         </div>

            <div id="page_counter">
            <p> {this.state.activePage}/4</p>
            </div>

         <div id="language_display">

            <p></p>

         </div>


      </div>
   </div>);
}
};

export default Table;
